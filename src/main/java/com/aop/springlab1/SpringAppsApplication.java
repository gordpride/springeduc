package com.aop.springlab1;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.logging.Logger;


@SpringBootApplication
public class SpringAppsApplication implements CommandLineRunner {
    private static final Logger logger = Logger.getLogger(String.valueOf(SpringAppsApplication.class));

    public static void main(String[] args) {
        SpringApplication.run(SpringAppsApplication.class, args);
    }

    public void run(String... args) {
        /*ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );*/

        // Discriminant discriminant = context.getBean("discriminantBean", Discriminant.class);
        // MaximumValue maximumValue = context.getBean("maxValueBean", MaximumValue.class);
        // MinimumValue minimumValue = context.getBean("minValueBean", MinimumValue.class);

        // logger.info(discriminant.findRoots());
        // logger.info(String.valueOf(maximumValue.findMaximum()));
        // logger.info(String.valueOf(minimumValue.findMinimum()));

        Discriminant discriminant = new Discriminant(1, 8, 9);
        logger.info(discriminant.findRoots());
    }
}
