package com.aop.springlab1;

public class MaximumValue {
    private int[] array;

    public MaximumValue(int[] array) {
        this.array = array;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int findMaximum() {
        int maxValue = Integer.MIN_VALUE;

        for (int j : array) {
            if (j > maxValue) {
                maxValue = j;
            }
        }

        return maxValue;
    }
}
