package com.aop.springlab1;

public class Discriminant {
    private int a;
    private int b;
    private int c;

    public Discriminant(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getC() {
        return c;
    }

    public void setC(int c) {
        this.c = c;
    }

    public String findRoots() {
        double discriminant = findDiscriminant();

        if (discriminant > 0) {
            return "First root = " + (-b + Math.sqrt(discriminant) / (2 * a)) + " Second root = " + (-b - Math.sqrt(discriminant) / (2 * a));
        } else if (discriminant == 0) {
            return "Discriminant equals zero. Root = " + (-(b / 2 * a));
        } else {
            return "Discriminant less zero. No roots.";
        }
    }

    private double findDiscriminant() {
        return Math.pow(b, 2) - 4 * a * c;
    }

}
