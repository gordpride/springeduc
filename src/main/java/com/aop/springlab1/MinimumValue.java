package com.aop.springlab1;

public class MinimumValue {
    private int[] array;

    public MinimumValue(int[] array) {
        this.array = array;
    }

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int findMinimum() {
        int minValue = Integer.MAX_VALUE;

        for (int j : array) {
            if (j < minValue) {
                minValue = j;
            }
        }

        return minValue;
    }
}
